package fr.univlittoral.dlabs;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="TBL_DEAL")

public class DODeal {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="TITLE", nullable=false)
	private String title;
	
	@Column(name="SHOP_NAME")
	private String shop_name;
	
	@Column(name="SHOP_LINK")
	private String shop_link;
	
	@Column(name="TEMPERATURE")
	private Integer temperature;
	
	@Column(name="CREATOR")
	private String creator;
	
	@Column(name="DATE")
	private Timestamp date;
	
	@Column(name="IMG_URL")
	private String img_url;
	
	public DODeal() {}

	//Getters & setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShop_name() {
		return shop_name;
	}

	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}

	public String getShop_link() {
		return shop_link;
	}

	public void setShop_link(String shop_link) {
		this.shop_link = shop_link;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
}