package fr.univlittoral.dlabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/deals")
public class ControllerREST {
	
	@Autowired
	private BODeal boDeal;
	
	@RequestMapping(method= RequestMethod.GET)
	public List<DTODeal> displayJSON() {
		final List<DTODeal> deals = boDeal.getData();
		return deals;
	}
	
	@RequestMapping(value = "/id={id}", method = RequestMethod.GET)
    public DTODetails displayDetails(@PathVariable Integer id){
		final DTODetails details = boDeal.getDetails(id);
		return details;
	}
	
	@RequestMapping(value = "/newDeal", method = RequestMethod.POST)
	public @ResponseBody void addNewDeal(DTODetails dtoDetails) {
		boDeal.create(dtoDetails);
	}
}