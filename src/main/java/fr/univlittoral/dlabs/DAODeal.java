package fr.univlittoral.dlabs;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

@Repository
public class DAODeal {
	
	@Autowired
	EntityManager entityManager;
	
	public List<DODeal> findAll() {
		// Creation de la requete
		Query q = entityManager.createQuery("from DODeal ORDER BY DATE DESC");
		
		// Recuperation de la liste des resultats
		List<DODeal> list = q.getResultList();
		
		// Affichage des résultats
		for (DODeal deal : list) {
			System.out.println((
			"Deal : [id] = "+ deal.getId () + "\t" +
			"[title] = "+ deal.getTitle () + "\t" +
			"[date] = "+ deal.getDate() + "\t"
			));
		}
		return q.getResultList();
	}
	
	public DODetails findByID(int id){
		Query q = entityManager.createQuery("from DODetails WHERE ID="+ id);
		return (DODetails) q.getSingleResult();
	}
	
	@Transactional
	public void addNewDeal(DODetails doDetails) {	
		//Enregistrement du deal
		entityManager.persist(doDetails);
	}
}