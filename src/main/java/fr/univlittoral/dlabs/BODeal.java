package fr.univlittoral.dlabs;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BODeal {
	
	@Autowired
	private DAODeal daoDeal;

	//Deals
	public List<DTODeal> getData(){
		final List<DODeal> deals = daoDeal.findAll();
		final List<DTODeal> listeFinale = new ArrayList<>();
		for (final DODeal daoDeal : deals) {
			final DTODeal dtoDeal = map(daoDeal);
			listeFinale.add(dtoDeal);
		}
		return listeFinale;
	}
	
	private DTODeal map(final DODeal doDeal) {
		final DTODeal dtoDeal = new DTODeal();
		dtoDeal.setId(doDeal.getId());
		dtoDeal.setTitle(doDeal.getTitle());
		dtoDeal.setShopName(doDeal.getShop_name());
		dtoDeal.setShopLink(doDeal.getShop_link());
		dtoDeal.setTemperature(doDeal.getTemperature());
		dtoDeal.setCreator(doDeal.getCreator());
		dtoDeal.setDateCreation(doDeal.getDate());
		dtoDeal.setImageURL(doDeal.getImg_url());
		return dtoDeal;
	}
	
	//Details
	public DTODetails getDetails(int id){
		DODetails doDetails = daoDeal.findByID(id);
		final DTODetails dtoDetails = map(doDetails);
		return dtoDetails;
	}
	
	private DTODetails map(final DODetails doDetails) {
		final DTODetails dtoDetails = new DTODetails();
		dtoDetails.setId(doDetails.getId());
		dtoDetails.setTitle(doDetails.getTitle());
		dtoDetails.setShopName(doDetails.getShop_name());
		dtoDetails.setShopLink(doDetails.getShop_link());
		dtoDetails.setOldPrice(doDetails.getPrice_old());
		dtoDetails.setNewPrice(doDetails.getPrice_new());
		dtoDetails.setPromoCode(doDetails.getPromo_code());
		dtoDetails.setTemperature(doDetails.getTemperature());
		dtoDetails.setCreator(doDetails.getCreator());
		dtoDetails.setDateCreation(doDetails.getDate());
		dtoDetails.setImageURL(doDetails.getImg_url());
		dtoDetails.setDescription(doDetails.getDescription());
		return dtoDetails;
	}
	
	//Create Deal
	public void create(DTODetails dtoDetails) {
		final DODetails doDetails = map(dtoDetails);
		daoDeal.addNewDeal(doDetails);
	}

	private DODetails map(DTODetails dtoDetails) {
		final DODetails doDetails = new DODetails();
		Date date = new Date();
		Timestamp timestamp =  new Timestamp(date.getTime());
		doDetails.setTitle(dtoDetails.getTitle());
		doDetails.setDescription(dtoDetails.getDescription());
		doDetails.setPrice_old(dtoDetails.getOldPrice());
		doDetails.setPrice_new(dtoDetails.getNewPrice());
		doDetails.setShop_name(dtoDetails.getShopName());
		doDetails.setPromo_code(dtoDetails.getPromoCode());
		doDetails.setImg_url(dtoDetails.getImageURL());
		doDetails.setDate(timestamp);
		
		return doDetails;
	}
}
