$(document).ready(function () {
    $.getJSON('http://localhost:8080/deals', function (json_data) {
        console.log(json_data);

        $.each(json_data, function (key, value) {
            let date = new Date(value.dateCreation);
            let temperature = (value.temperature ? value.temperature : 0);
            let author = (value.creator ? value.creator : "Unknown");

            $('#deals').append("<div class='row' id=item" + key + ">");
            $('#item' + key)
                .append($("<div class='col'>")
                    .append($("<img class='mx-auto d-block' width='120' height='120' alt='Image article' src='" + value.imageURL + "''>")))
                .append($("<div class='col-6'>")
                    .append($("<h2>").append(temperature + "°"))
                    .append($("<a class='h6' id='detailed-link' href='details.html?id=" + value.id + "'>").append(value.title))
                    .append($("<p>").append(author + " | " + value.shopName)))
                .append($("<div class='col text-right'>")
                    .append($("<p id='date'>").append(date.toLocaleDateString()))
                    .append($("<a class='btn btn-primary' role='button' id='look' href='" + value.shopLink + "'>Voir</a>")))
        })
    });
});