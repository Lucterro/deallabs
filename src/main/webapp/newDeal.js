$(document).ready(function () {
    console.log("Ready");

    $('#submit').click(function () {
        let data = {};
        data.title = $('#title').val();
        data.description = $('#description').val();
        data.oldPrice = $('#originalPrice').val();
        data.newPrice = $('#newPrice').val();
        data.shopName = $('#shopName').val();
        data.promoCode = $('#promoCode').val();
        data.imageURL = $('#imageURL').val();
        console.log(data);

        $.ajax({
            type: "POST",
            url: 'http://localhost:8080/deals/newDeal',
            dataType: 'json',
            data: data,
            success: function () {
                window.location ="http://localhost:63342/dlabs/src/main/webapp/index.html";
            }
        });
    });
});
