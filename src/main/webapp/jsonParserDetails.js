$(document).ready(function () {
    const queryString = window.location.search;
    const URLParams = new URLSearchParams(queryString);
    const id = URLParams.get('id');

    $.getJSON('http://localhost:8080/deals/id=' + id, function (data) {
        console.log(data);
        let date = new Date(data.dateCreation);
        let temperature = (data.temperature ? data.temperature : 0);
        let author = (data.creator ? data.creator : "Unknown");
        let promoCode = (data.promoCode ? data.promoCode : "Aucun code promotionel");
        let remise = (((data.oldPrice - data.newPrice) * 100) / data.oldPrice).toFixed();

        $('#deal').append("<div class='row' id=itemDetails>");
        $('#itemDetails')
            .append($("<div class='col'>")
                .append($("<img class='mx-auto d-block' width='120' height='120' alt='Image article' src='" + data.imageURL + "'>")))
            .append($("<div class='col-6'>")
                .append($("<h2>").append(temperature + "°"))
                .append($("<a class='h6' id='detailed-link' href='details.html?id=" + data.id + "'>").append(data.title))
                .append($("<p>").append(author + " | " + data.shopName)))
            .append($("<div class='col text-right'>")
                .append($("<p id='date'>").append(date.toLocaleDateString()))
                .append($("<a class='btn btn-primary' role='button' id='look' href='" + data.shopLink + "'>Voir</a>")));

        $('#deal')
            .append($("<div class='row justify-content-center' id='promoCodeDisplay'>")
                .append($("<h6>").append(promoCode)))
            .append($("<div class='row justify-content-center' id='priceDisplay'>")
                .append($("<h6>").append(data.oldPrice + " > " + data.newPrice + " (" + remise + " % de remise)")))
            .append($("<div class='row text-left' id='descriptionDisplay'>")
                .append($("<p>").append(data.description)));
    })
});